(ns plf03.core)

(defn funcion-comp-1
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (* 2 x))
        z (comp  g f )]
    (z 2)))

(defn funcion-comp-2
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (* 2 x))
        z (comp  f g)]
    (z 3)))



(defn funcion-comp-3
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (* 2 x))
        h (fn [x] (+ 3 x))
        z (comp  f h g)]
    (z 5)))

(defn funcion-comp-4
  []
  (let [f (fn [x] (/ 2 x))
        g (fn [x] (* 2 x))
        h (fn [x] (+ 3 x))
        z (comp  f h g)] 
    (z 3)))

(defn funcion-comp-5
  []
  (let [f (fn [x] (dec  x))
        g (fn [x] (dec' x))
        h (fn [x] (+ 3 x))
        z (comp  f h g)] 
(z 7)))

(defn funcion-comp-6
  []
  (let [f (fn [x] (dec x))
        g (fn [x] (dec x))
        h (fn [x] (inc x))
        z (comp  f h g)]
    (z 1)))

(defn funcion-comp-7
  []
  (let [f (fn [x] (inc (- 6 x)))
        g (fn [x] ( + 10 x))
        h (fn [x] (inc x))
        z (comp  h g f)]
    (z 10)))

(defn funcion-comp-8
  []
  (let [f (fn [x] (inc (- 6 x)))
        g (fn [x] (+ 10 x))
        h (fn [x] (inc x))
        i (fn [x] (dec (+ 7 x)))
        z (comp i h g f)]
    (z 100)))

(defn funcion-comp-9
  []
  (let [f (fn [x] (filter even? x))
        g (fn [x] (map inc x))
        z (comp  g f )]
    (z [1 2 3 4 5 6])))

(defn funcion-comp-10
  []
  (let [f (fn [x] (filter even? x))
        g (fn [x] (map dec x))
        z (comp  g f)]
    (z [7 6 5 4 3 2])))

  (defn funcion-comp-11
  []
  (let [f (fn [x] (filter even? x))
        g (fn [x] (map max x))
        z (comp  g f)]
    (z [7 6 5 4 3 2])))

(defn funcion-comp-12
  []
  (let [f (fn [x] (filter even? x))
        g (fn [x] (map min x))
        z (comp   f g)]
    (z [1 3 5 7 9])))


(funcion-comp-1)
(funcion-comp-2)
(funcion-comp-3)
(funcion-comp-4)
(funcion-comp-5)
(funcion-comp-6)
(funcion-comp-7)
(funcion-comp-8)
(funcion-comp-9)
(funcion-comp-10)
(funcion-comp-11)
(funcion-comp-12)


(defn funcion-comp-13
  []
  (let [f (fn [x] (take-last 2 x))
        g (fn [x] (map inc x))
        h (fn [x] (filter number? x))
        z (comp  f g h)]
    (z [1 3 "af" 5 :a 7 9])))

(defn funcion-comp-14
  []
  (let [f (fn [x] (filter number? x))
        g (fn [x] (map inc x))
        h (fn [x] (group-by even? x))
        z (comp h g f)]
    (z [1 3 "af" 5 :a 7 9])))

(defn funcion-comp-15
  []
  (let [f (fn [x] (filter number? x))
        g (fn [x] (map inc x))
        h (fn [x] (remove pos? x))
        z (comp h g f)]
    (z [1 3 "af" 5 :a 7 9])))

(defn funcion-comp-16
  []
  (let [f (fn [x] (filter number? x))
        g (fn [x] (map dec x))
        h (fn [x] (remove pos? x))
        z (comp h g f)]
    (z [1 3 "af" 5 :a 7 9])))

(defn funcion-comp-17
  []
  (let [g (fn [x] (map dec x))
        h (fn [x] (filter number? x))
        z (comp g h)]
    (z [1 3 "af" 5 :a 7 9])))

(defn funcion-comp-18
  []
  (let [g (fn [x] (first x))
        h (fn [x] (filter number? x))
        i (fn [x] (map inc x))
        z (comp g i h)]
    (z [1 3 "af" 5 :a 7 9])))

(defn funcion-comp-19
  []
  (let [g (fn [x] (filter number? x))
        h (fn [x] (map inc x))
        i (fn [x] (drop 2 x))
        z (comp i h g)]
    (z [1 3 "af" 5 :a 7 9])))

(defn funcion-comp-20
  []
  (let [g (fn [x] (take-last 2 x))
        h (fn [x] (map inc x))
        i (fn [x] (filter number? x))
        z (comp g h i)]
    (z [1 3 "af" 5 :a 7 9])))

(funcion-comp-13)
(funcion-comp-14)
(funcion-comp-15)
(funcion-comp-16)
(funcion-comp-17)
(funcion-comp-18)
(funcion-comp-19)
(funcion-comp-20)


(defn funcion-complement-1
  []
  (let [g (fn [x] (map  even? x))
        z (complement g)]
    (z '(1 2 3 4 5))))

(defn funcion-complement-2
  []
  (let [g (fn [x] (float? x))
       z (complement g)]
    (z [1 2 3])))

(defn funcion-complement-3
  []
  (let [g (fn [x] (boolean? x))
        z (complement g)]
    (z true)))

(defn funcion-complement-4
  []
  (let [g (fn [x] (pos? x))
        z (complement g)]
    (z 1)))

(defn funcion-complement-5
  []
  (let [g (fn [x] (neg? x))
        z (complement g)]
    (z -2)))

(defn funcion-complement-6
  []
  (let [g (fn [x] (number? x))
        z (complement g)]
    (z :b)))

(defn funcion-complement-7
  []
  (let [g (fn [x] (rational? x))
        z (complement g)]
    (z 6/7)))


(defn funcion-complement-8
  []
  (let [g (fn [x] (rational? (vector x)))
        z (complement g)]
    (z [1 2 3 4 5 6 7 0.5])))

(defn funcion-complement-9
  []
  (let [g (fn [x] (char? x))
        z (complement g)]
    (z \w)))

(defn funcion-complement-10
  []
  (let [g (fn [x] (true? x))
        z (complement g)]
    (z false)))

(funcion-complement-1)
(funcion-complement-2)
(funcion-complement-3)
(funcion-complement-4)
(funcion-complement-5)
(funcion-complement-6)
(funcion-complement-7)
(funcion-complement-8)
(funcion-complement-9)
(funcion-complement-10)

(defn funcion-complement-11
  []
  (let [g (fn [x] (integer? x))
        z (complement g)]
    (z 1.0)))

(defn funcion-complement-12
  []
  (let [g (fn [x] (zero? x))
        z (complement g)]
    (z 0)))

(defn funcion-complement-13
  []
  (let [g (fn [x] (not-any? odd? x))
        z (complement g)]
(z [1 2 3 4])))

(defn funcion-complement-14
  []
  (let [g (fn [x] (nil? x))
        z (complement g)]
    (z nil)))

(defn funcion-complement-15
  []
  (let [g (fn [x] (ratio? x))
        z (complement g)]
    (z 2/7)))


(defn funcion-complement-16
  []
  (let [g (fn [x] (neg-int? x))
        z (complement g)]
    (z 2/7)))

(defn funcion-complement-17
  []
  (let [g (fn [x] (pos-int? x))
        z (complement g)]
    (z 2)))

(defn funcion-complement-18
  []
  (let [g (fn [x] (even? x))
        z (complement g)]
(z 2)))

(defn funcion-complement-19
  []
  (let [g (fn [x] (distinct? x))
        z (complement g)]
    (z [ 33 1 2 33])))

(defn funcion-complement-20
  []
  (let [g (fn [x] (vector? x))
        z (complement g)]
    (z { 1 2 3 4})))

(funcion-complement-11)
(funcion-complement-12)
(funcion-complement-13)
(funcion-complement-14)
(funcion-complement-15)
(funcion-complement-16)
(funcion-complement-17)
(funcion-complement-18)
(funcion-complement-19)
(funcion-complement-20)

(defn funcion-constantly-1
  []
  (let [f (vector 1 2 3 4 5)  
        z ( constantly f )]
    (z #{ 1 2 3})))

(defn funcion-constantly-2
  []
  (let [f true
        z (constantly f)]
    (z 1)))

(defn funcion-constantly-3
  []
  (let [f {:a 1 :b 2}
        z (constantly f)]
    (z #{1 2 3})))

(defn funcion-constantly-4
  []
  (let [f (count [1 2 3 ])
        z (constantly f)]
(z { })))


(defn funcion-constantly-5
  []
  (let [f (+ 12 3 4 5)
        z (constantly f)]
    (z {})))


(defn funcion-constantly-6
  []
  (let [f (max 12 3 4 5)
        z (constantly f)]
(z 5)))

(defn funcion-constantly-7
  []
  (let [f (min 12 3 4 5)
        z (constantly f)]
(z 2.2)))

(defn funcion-constantly-8
  []
  (let [f (- 34 2)
        z (constantly f)]
    (z 2)))


(defn funcion-constantly-9
  []
  (let [f (* 34 2)
        z (constantly f)]
    (z 2)))

(defn funcion-constantly-10
  []
  (let [f (rational? 12)
        z (constantly f)]
    (z 2)))




(funcion-constantly-1)
(funcion-constantly-2)
(funcion-constantly-3)
(funcion-constantly-4)
(funcion-constantly-5)
(funcion-constantly-6)
(funcion-constantly-7)
(funcion-constantly-8)
(funcion-constantly-9)
(funcion-constantly-10)

(defn funcion-constantly-11
  []
  (let [f (sequential? [1 2 3])
        z (constantly f)]
    (z [2 4 7])))

(defn funcion-constantly-12
  []
  (let [f (remove pos? [1 2 3])
        z (constantly f)]
(z [7 4 7])))

(defn funcion-constantly-13
  []
  (let [f (remove neg? [1 2 3])
        z (constantly f)]
(z [7 4 7])))

(defn funcion-constantly-14
  []
  (let [f (first '(:1 1 :2 2))
        z (constantly f)]
(z [7 4 7 7])))

(defn funcion-constantly-15
  []
  (let [f (odd? 1)
        z (constantly f)]
    (z 9)))

(defn funcion-constantly-16
  []
  (let [f (/ 5 1)
        z (constantly f)]
    (z true)))

(defn funcion-constantly-17
  []
  (let [f (map inc [1 2 3 4 5])
        z (constantly f)]
    (z true)))

(defn funcion-constantly-18
  []
  (let [f (map + [1 2 3 4 5] [1 2 3 4 5])
        z (constantly f)]
    (z 2))) 

(defn funcion-constantly-19
  []
  (let [f (list 'a 'b 'c 'd 'e 'f 'g)
        z (constantly f)]
     (z 9))) 

(defn funcion-constantly-20
  []
  (let [f (let [x 1 y 2]
            (list x y))
        z (constantly f)]
    (z 9)))
  
  
(funcion-constantly-11)
(funcion-constantly-12)
(funcion-constantly-13)
(funcion-constantly-14)
(funcion-constantly-15)
(funcion-constantly-16)
(funcion-constantly-17)
(funcion-constantly-18)
(funcion-constantly-19)
(funcion-constantly-20)

(defn funcion-every-pred-1
  []
  (let [f (fn [x] (number? x))
        z (every-pred f )]
   (z 10) ))

(defn funcion-every-pred-2
  []
  (let [f (fn [x] (odd? x))
        z (every-pred f )]
   (z 3) ))

(defn funcion-every-pred-3
  []
  (let [f (fn [x] (int? x))
        z (every-pred f)]
    (z 8)))

(defn funcion-every-pred-4
  []
  (let [f (fn [x] (every-pred number? odd? x))
        z (every-pred f)]
    (z 12 3)))

(defn funcion-every-pred-5
  []
  (let [f (fn [x] (every-pred vector? x))
        z (every-pred f)]
    (z [1 2 3 4])))

(defn funcion-every-pred-6
  []
  (let [f (fn [x] ( map x))
        z (every-pred f)]
    (z {:a 1 :b 2})))

(defn funcion-every-pred-7
  []
  (let [f (fn [x] (filter odd? x))
        z (every-pred f)]
    (z [3 9 7])))

(defn funcion-every-pred-8
  []
  (let [f (fn [x] (even? x))
        z (every-pred f)]
    (z 1 2 3.4 5)))

(defn funcion-every-pred-9
  []
  (let [f (fn [xs] (char? xs))
        z (every-pred f)]
    (z \q \s \q \a)))

(defn funcion-every-pred-10
  []
  (let [f (fn [xs] (pos? xs))
        z (every-pred f)]
    (z 1 4 6 -90)))

(funcion-every-pred-1)
(funcion-every-pred-2)
(funcion-every-pred-3)
(funcion-every-pred-4)
(funcion-every-pred-5)
(funcion-every-pred-6)
(funcion-every-pred-7)
(funcion-every-pred-8)
(funcion-every-pred-9)
(funcion-every-pred-10)

(defn funcion-every-pred-11
  []
  (let [f (fn [x] (float? x))
        z (every-pred f)]
    (z 1.0 0.3 4N)))

(defn funcion-every-pred-12
  []
  (let [f (fn [xs] (neg-int? xs))
        z (every-pred f)]
    (z -2 -3.4 -1 -100.40)))

(defn funcion-every-pred-13
  []
  (let [f (fn [x] (symbol? x))
        z (every-pred f)]
    (z 'q 'w 'e 'r 'q)))


(defn funcion-every-pred-14
  []
  (let [f (fn [xs] (number? xs))
        g (fn [xs] (filter neg-int? xs))
        z (every-pred f g)]
    (z 1 23 -2 -1)))

(defn funcion-every-pred-15
  []
  (let [f (fn [x] (float? x))
        g (fn [x] (number? x))
        z (every-pred f g)]
    (z 1.3 3.5 4.4)))

(defn funcion-every-pred-16
  []
  (let [f (fn [xs] (neg-int? xs))
        g (fn [xs] (number? xs))
        z (every-pred f g)]
    (z -1 -3 -5 -6)))

(defn funcion-every-pred-17
  []
  (let [f (fn [x] (seq? x))
        g (fn [x] (list? x))
        z (every-pred f g)]
    (z '(1 2 3 4))))

(defn funcion-every-pred-18
  []
  (let [l (fn [xs] (char? (first xs)))
        f (fn [xs] (seq? xs))
        g (fn [xs] (list? xs))
        z (every-pred l f g)]
    (z '(\q \w \e))))

(defn funcion-every-pred-19
  []
  (let [f (fn [x] (filter char? x))
        z (every-pred f)]
    (z [\q \w \e \r 1])))

(defn funcion-every-pred-20
  []
  (let [f (fn [xs] (seq? xs))
        g (fn [xs] (vector? xs))
        z (every-pred f g)]
    (z [1 2 3 4])))

(funcion-every-pred-11)
(funcion-every-pred-12)
(funcion-every-pred-13)
(funcion-every-pred-14)
(funcion-every-pred-15)
(funcion-every-pred-16)
(funcion-every-pred-17)
(funcion-every-pred-18)
(funcion-every-pred-19)
(funcion-every-pred-20)

(defn funcion-fnil-1
[]
  (let [f (fn [x] (* 2 x))
        g (fn [x] (pos? x))
        z (fnil f g)]
    (z 6)))

(defn funcion-fnil-2
[]
  (let [f (fn [x] (< x 10))
        g (fn [x] (> x 10))
        z (fnil f g)]
    (z 10)))

(defn funcion-fnil-3
  []
  (let [f (fn [x] (filter pos? x))
        g (fn [x] (* 3 x))
        z (fnil f g)]
    (z [1 2 3 4 -5 ])))

(defn funcion-fnil-4
  []
  (let [f (fn [x] (neg? x))
        g (fn [x] (* 3 x))
         z (fnil f g)]
    (z 5)))

(defn funcion-fnil-5
  []
  (let [f (fn [x] (* 3 x))
        g (fn [x] (double? x))
         z (fnil f g)]
    (z 3)))

(defn funcion-fnil-6
  []
  (let [f (fn [x y] (if (<= x y)(+ x y)(- x y)))
        z (fnil f 50)]
    (z nil 10)))

(defn funcion-fnil-7
  []
  (let [f (fn [x y z] (* (+ x y z) (+ x y z)))
        z (fnil f 4 2)]
    (z nil nil 5))) 

(funcion-fnil-1)
(funcion-fnil-2)
(funcion-fnil-3)
(funcion-fnil-4)
(funcion-fnil-5)
(funcion-fnil-6)
(funcion-fnil-7)

(defn funcion-fnil-8
  []
  (let [f (fn [x y z] (if (== x y z) (+ x y z) (+ x y z)))
        z (fnil f 4 4)]
    (z nil nil 4)))

(defn funcion-fnil-9
  []
  (let [f (fn [x y] ( if (pos? x) (+ x y) (- x y) ))
        z (fnil f 4 )]
    (z nil 4)))

(defn funcion-fnil-10
  []
  (let [f (fn [x y] ( if (neg? x) (/ x y) (- x y) ))
        z (fnil f 4 )]
    (z nil 3)))

(defn funcion-fnil-11
  []
  (let [f (fn [x y] (if (zero? x) (/ x y) (* x y)))
        z (fnil f 4)]
    (z nil 0)))

(defn funcion-fnil-12
  []
  (let [f (fn [x y z] (if (== x y z) (- x y z) (/ x y z )))
        z (fnil f 4 4)]
(z nil nil 5)))

(funcion-fnil-8)
(funcion-fnil-9)
(funcion-fnil-10)
(funcion-fnil-11)
(funcion-fnil-12)